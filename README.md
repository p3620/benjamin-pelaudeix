# Welcome aboard! 👋

![Banner](./public/img/banner/logo-large-white-bg.jpg)

## Presentation

My name is Benjamin PELAUDEIX, I'm 20 and I'm student at La Rochelle University as apprendiceship Full-Stack developer.

Out of coding, I'm a sport fan especially about 🏀, 🏉 and 🏸. I also like 🎬 and 🍻.

## Working experience

- 04/2021 -> 06/2021 : Intership at Legrand 🧑‍💻
- 09/2021 -> now : Apprendiceship at Emakina.FR 🧑‍💻

## Hard skills

### Front-End

<img src="./public/img/logo/tech/html5-brands.png" width="auto" height="50px"> 
<img src="./public/img/logo/tech/css3-alt-brands.png" width="auto" height="50px"> 
<img src="./public/img/logo/tech/js-brands.png" width="auto" height="50px"> 
<br>
<img src="./public/img/logo/tech/react-brands.png" width="auto" height="50px"> 


### Back-End

<img src="./public/img/logo/tech/php-brands.png" width="auto" height="50px"> 
<img src="./public/img/logo/tech/symfony-brands.png" width="auto" height="50px"> 
<br>
<img src="./public/img/logo/tech/node-js-brands.png" width="auto" height="50px"> 
<br>
<img src="./public/img/logo/tech/database-solid.png" width="auto" height="50px"> 

## Soft skills

*To be written...* 😁

## Working environment

<img src="./public/img/logo/tech/apple-brands.png" width="auto" height="20px"> MacBook Air (M1, 2020) 8Go RAM

## Learn more from me

If you need more information about myself, my website is on : [CLICK HERE](https://benjaminpelaudeix.fr)

## Copyright

© Benjamin PELAUDEIX - 2021 - All rights reserved
